#include <Arduino.h>
#include <Servo.h>
#include <HC_SR04.h>

/*
 * Jason Russo
 */

/***************************** Pin Values ********************************/

// Digital pin constants
#define   ECHO_PIN                      2
#define   TRIG_PIN                      3
#define   RIGHT_LED_PIN                 4
#define   LEFT_LED_PIN                  5
#define   ECHO_SERVO_MOTOR              6
#define   RIGHT_MOTOR_PIN               10
#define   LEFT_MOTOR_PIN                11

// Analog pin constants
#define   LIGHT_SENSOR_RIGHT_PIN        4
#define   LIGHT_SENSOR_LEFT_PIN         2
#define   THRESHOLD_POTENTIOMETER_PIN   0

/************************** End Pin Values ******************************/


/************************** Begin Objects *******************************/

// Motor objects
Servo panMotor;                         // Microservo with distance sensor
Servo leftDriveMotor;
Servo rightDriveMotor;

// Sensor objects
HC_SR04 sensor(TRIG_PIN, ECHO_PIN, digitalPinToInterrupt(ECHO_PIN));

/*************************** End Objects ********************************/


/********************** Constants and Variables *************************/

// Direction constants
const int STOP                  = 0;
const int GO_BACKWARD           = 1;
const int GO_FORWARD            = 2;
const int TURN_SLOW_LEFT        = 3;
const int TURN_FAST_LEFT        = 4;
const int TURN_SLOW_RIGHT       = 5;
const int TURN_FAST_RIGHT       = 6;

// Motor constants
const int DRIVE_NEUTRAL         = 90;

const int DRIVE_LEFT_FORWARD    = 60;
const int DRIVE_RIGHT_FORWARD   = 120;

const int DRIVE_LEFT_BACKWARD   = 120;
const int DRIVE_RIGHT_BACKWARD  = 60;

// Pan Servo constants
const int PAN_CENTER            = 90;
const int PAN_CLOSE_LEFT        = 135;
const int PAN_CLOSE_RIGHT       = 45;
const int PAN_WIDE_LEFT         = 0;
const int PAN_WIDE_RIGHT        = 180;

// Collision properties
const int collisionThresh       = 20;                     // Threshold for obstacles (in cm)
const int backupDist            = collisionThresh / 2;    // In cm
int leftDistance, rightDistance;                          // Distances on either side

// Photo sensor values
const int targetLightDiff       = 75;

int       photoLeftVal;
int       photoRightVal;

boolean   foundTarget           = false;

// Bluetooth properties
const byte  RC_CTRL_STOP        = 0x01;
const byte  RC_CTRL_FORWARD     = 0x02;
const byte  RC_CTRL_BACKWARD    = 0x03;
const byte  RC_CTRL_LEFT        = 0x04;
const byte  RC_CTRL_RIGHT       = 0x05;

boolean     serialDebugMode     = true;
boolean     rcControlMode       = false;
byte        rcCommand           = (byte) 0x00;

/********************* End Constants and Variables ***********************/

/******************** Standard Arduino Entry-points **********************/

void setup() {
  // Initialize sensors
  pinMode(LIGHT_SENSOR_LEFT_PIN, INPUT);
  pinMode(LIGHT_SENSOR_RIGHT_PIN, INPUT);
  pinMode(THRESHOLD_POTENTIOMETER_PIN, INPUT);

  pinMode(LEFT_LED_PIN, OUTPUT);
  pinMode(RIGHT_LED_PIN, OUTPUT);
  
  // Initialize distance sensor servo
  panMotor.attach(ECHO_SERVO_MOTOR);      // Attach to pin
  panMotor.write(90);                     // Center servo

  leftDriveMotor.attach(LEFT_MOTOR_PIN);
  rightDriveMotor.attach(RIGHT_MOTOR_PIN);

  leftDriveMotor.write(DRIVE_NEUTRAL);    // Stop drive wheels
  rightDriveMotor.write(DRIVE_NEUTRAL);   // Stop drive wheels
  
  Serial.begin(9600);
  while (!Serial) continue;
    
  // Initialize distance sensor
  sensor.begin();
  sensor.start();
}

void loop() {
  if (rcControlMode) {
    if (rcCommand) {
      processRcCommand();
      rcCommand = (byte) 0x00;
    }
  } else {
    if (!checkForCollisions(foundTarget)) {
      foundTarget = checkLighting();
    }
  }
}

void serialEvent() {
  char inputChar;

  if (Serial.available()) {
    inputChar = (char) Serial.read();
    parseInputCommand(inputChar);
  }
}

/****************** End Standard Arduino Entry-points ********************/

/*
  Parses character commands coming from the Serial input.
*/
void parseInputCommand(char command) {
  switch (command) {
    case 's':
      rcControlMode = true;
      rcCommand = RC_CTRL_STOP;
      Serial.println("Manual stop");
      break;
    case 'a':
      rcControlMode = false;
      Serial.println("Automatic mode");
      break;
    case 'f':
      rcCommand = RC_CTRL_FORWARD;
      Serial.println("Manual foward");
      break;
    case 'b':
      rcCommand = RC_CTRL_BACKWARD;
      Serial.println("Manual backward");
      break;
    case 'l':
      rcCommand = RC_CTRL_LEFT;
      Serial.println("Manual left");
      break;
    case 'r':
      rcCommand = RC_CTRL_RIGHT;
      Serial.println("Manual right");
      break;
    case 'd':
      serialDebugMode = !serialDebugMode;
  }
}

/*
  Processes remote control commands into robot actions.
*/
void processRcCommand() {
  switch (rcCommand) {
    case RC_CTRL_STOP:
      moveDirection(STOP);
      break;
    case RC_CTRL_FORWARD:
      moveDirection(GO_FORWARD);
      break;
    case RC_CTRL_BACKWARD:
      moveDirection(GO_BACKWARD);
      break;
    case RC_CTRL_LEFT:
      moveDirection(TURN_SLOW_LEFT);
      break;
    case RC_CTRL_RIGHT:
      moveDirection(TURN_SLOW_RIGHT);
      break;
  }
}

/*
  This function compares the photo resistor readings and tells the bot which
  direction to move.  When done, returns true or false indicating whether
  target light source has been reached.
*/
bool checkLighting() {
  int lightSensorDiff;
  int potThreshTop, potThreshFloor;             // Potentiometer values

  photoLeftVal = analogRead(LIGHT_SENSOR_LEFT_PIN);
  delay(10);

  photoRightVal = analogRead(LIGHT_SENSOR_RIGHT_PIN);
  delay(10);

  potThreshTop = analogRead(THRESHOLD_POTENTIOMETER_PIN);
  delay(10);

  lightSensorDiff = abs(photoLeftVal - photoRightVal);
  potThreshFloor = potThreshTop >> 2;

  // Print photo status
  if (serialDebugMode) {
    Serial.print("Photo sensor readings - ");  
    Serial.print("Left = ");
    Serial.print(photoLeftVal);
    Serial.print("; Right = ");
    Serial.print(photoRightVal);
    Serial.print("; Diff = ");
    Serial.print(lightSensorDiff);
    Serial.print("/");
    Serial.print(targetLightDiff);
    Serial.print("; Thresh = ");
    Serial.print(potThreshTop);
    Serial.print(":");
    Serial.print(potThreshFloor);
    Serial.print("; Move: ");
  }

  // Decide next move
  if (photoLeftVal <= potThreshTop && photoRightVal <= potThreshTop &&
      photoLeftVal >= potThreshFloor && photoRightVal >= potThreshFloor &&
      lightSensorDiff <= targetLightDiff) {
    // Move forward
    if (serialDebugMode) Serial.print("Forward");
    digitalWrite(LEFT_LED_PIN, HIGH);
    digitalWrite(RIGHT_LED_PIN, HIGH);
    moveDirection(GO_FORWARD);
  } else if (photoLeftVal >= potThreshTop && photoRightVal >= potThreshTop &&
      lightSensorDiff <= targetLightDiff) {
    // Reached light source
    if (serialDebugMode) Serial.println("Stop");
    digitalWrite(LEFT_LED_PIN, LOW);
    digitalWrite(RIGHT_LED_PIN, LOW);
    moveDirection(STOP);
    return true;
  } else if (photoLeftVal > photoRightVal && photoRightVal >= potThreshFloor) {
    // Turn left
    if (serialDebugMode) Serial.print("Left");
    digitalWrite(LEFT_LED_PIN, HIGH);
    digitalWrite(RIGHT_LED_PIN, LOW);
    moveDirection(TURN_SLOW_LEFT);
  } else if (photoRightVal > photoLeftVal && photoLeftVal >= potThreshFloor) {
    // Turn right
    if (serialDebugMode) Serial.print("Right");
    digitalWrite(LEFT_LED_PIN, LOW);
    digitalWrite(RIGHT_LED_PIN, HIGH);
    moveDirection(TURN_SLOW_RIGHT);
  } else {
    // Only other condition means we're lost. What do we do?
    if (serialDebugMode) Serial.print("Lost");
    moveDirection(STOP);
    
    for (int i = 0; i < 10; i++) {
      digitalWrite(LEFT_LED_PIN, HIGH);
      digitalWrite(RIGHT_LED_PIN, HIGH);
      delay(100);
      digitalWrite(LEFT_LED_PIN, LOW);
      digitalWrite(RIGHT_LED_PIN, LOW);
      delay(200);
    }

    // Spin around
    if (photoLeftVal > photoRightVal) {
      moveDirection(TURN_FAST_LEFT);
      delay(500);
    } else if (photoRightVal > photoLeftVal) {
      moveDirection(TURN_FAST_RIGHT);
      delay(500);
    } else {
      moveDirection(TURN_FAST_LEFT);
      delay(1000);
    }

    moveDirection(GO_FORWARD);
    delay(500);
    moveDirection(STOP);
  }

  if (serialDebugMode) Serial.println();
  return false;
}

/*
  This function checks the echo monitor for whether bot is too close to an
  obstacle. If it is, it turns the monitor head to seek a clearer path.

  Params:       foundTarget           Indicator to skip collision checking
                                      (Stops bot from constantly bobbing at target)
  
  Returns:      boolean               True = collision detected ahead
                                      False = all clear ahead
*/
bool checkForCollisions(bool foundTarget) {
  bool collisionFound;
  uint32_t distanceCenter;
  int distanceRight, distanceLeft;

  if (foundTarget) {
    return false;
  }

  if (sensor.isFinished()) {
    distanceCenter = sensor.getRange();
    
    if (serialDebugMode) {
      Serial.print("Echo sensor readings - ");
      Serial.print("Center: ");
      Serial.print(distanceCenter);
      Serial.print(" cm");
      Serial.print("; ");
    }

    if (distanceCenter > collisionThresh) {
      collisionFound = false;
      if (serialDebugMode) Serial.print("Path clear, continue...");
    } else {
      collisionFound = true;
      moveDirection(STOP);

      distanceCenter = pingBlocking();

      if (distanceCenter < backupDist) {
        collisionFound = true;
        if (serialDebugMode) Serial.print("Too close. Let's back up...");
        moveDirection(GO_BACKWARD);
        delay(200);
      } else {
        scanDirections(false, distanceLeft, distanceRight);

        if (distanceLeft > distanceRight && distanceLeft > backupDist) {
          if (serialDebugMode) Serial.print("Left turn!");
          moveDirection(TURN_SLOW_LEFT);
          delay(1000);
        } else if (distanceRight > distanceLeft && distanceRight > backupDist) {
          if (serialDebugMode) Serial.print("Right turn!");
          moveDirection(TURN_SLOW_RIGHT);
          delay(1000);
        } else {
          if (serialDebugMode) Serial.print("Let's look wider...");
          scanDirections(true, distanceLeft, distanceRight);

          if (distanceLeft > distanceRight && distanceLeft > backupDist) {
            if (serialDebugMode) Serial.print("Left turn!");
            moveDirection(TURN_FAST_LEFT);
            delay(1000);
          } else if (distanceRight > distanceLeft && distanceRight > backupDist) {
            if (serialDebugMode) Serial.print("Right turn!");
            moveDirection(TURN_FAST_RIGHT);
            delay(1000);
          } else {
            // Back up and try again
            moveDirection(GO_BACKWARD);
            delay(500);
          }
        }
      }

      panMotor.write(90);
    }

    if (serialDebugMode) Serial.println();
    sensor.start();
    return collisionFound;
  } else {
    return false;
  }
}

int scanDirections(bool wideScan, int& leftDist, int& rightDist) {
  int scanAngle;

  scanAngle = (wideScan ? 90 : 45);

  rightDist = scanDistance(90 - scanAngle);
  // panMotor.write(90 - scanAngle);
  // delay(500);
  // rightDist = pingBlocking();
  
  // if (serialDebugMode) {
  //   Serial.print("Right distance = ");
  //   Serial.print(rightDist);
  //   Serial.print(" cm; ");
  // }
  delay(500);

  leftDist = scanDistance(90 + scanAngle);
  // panMotor.write(90 + scanAngle);
  // delay(500);
  // leftDist = pingBlocking();

  // if (serialDebugMode) {
  //   Serial.print("Left distance = ");
  //   Serial.print(leftDist);
  //   Serial.print(" cm; ");
  // }  
  delay(500);
}

int scanDistance(int scanAngle) {
  int distance;

  panMotor.write(scanAngle);
  delay(500);

  distance = pingBlocking();
  
  if (serialDebugMode) {
    Serial.print("Right distance = ");
    Serial.print(distance);
    Serial.print(" cm; ");
  }

  return distance;
}

long pingBlocking() {  
  sensor.start();
  
  while (!sensor.isFinished()) {
    delay(100);
  }

  return sensor.getRange();
}

void moveDirection(int turnDirection) {
  switch (turnDirection) {
    case STOP:
      turnWheels(DRIVE_NEUTRAL, DRIVE_NEUTRAL, -1);
      break;
    case GO_FORWARD:
      turnWheels(DRIVE_LEFT_FORWARD, DRIVE_RIGHT_FORWARD, -1);
      break;
    case GO_BACKWARD:
      turnWheels(DRIVE_LEFT_BACKWARD, DRIVE_RIGHT_BACKWARD, -1);
      break;
    case TURN_SLOW_LEFT:
      turnWheels(DRIVE_NEUTRAL, DRIVE_RIGHT_FORWARD, -1);
      break;
    case TURN_SLOW_RIGHT:
      turnWheels(DRIVE_LEFT_FORWARD, DRIVE_NEUTRAL, -1);
      break;
    case TURN_FAST_LEFT:
      turnWheels(DRIVE_LEFT_BACKWARD, DRIVE_RIGHT_FORWARD, -1);
      break;
    case TURN_FAST_RIGHT:
      turnWheels(DRIVE_LEFT_FORWARD, DRIVE_RIGHT_BACKWARD, -1);
      break;
  }
}

void turnWheels(int leftWheelTurn, int rightWheelTurn, int timeInMillis) {
  leftDriveMotor.write(leftWheelTurn);
  rightDriveMotor.write(rightWheelTurn);

  if (timeInMillis > 0) {
    delay(timeInMillis);
  }
}